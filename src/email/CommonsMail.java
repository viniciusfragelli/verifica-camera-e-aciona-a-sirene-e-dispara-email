/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package email;

import com.sun.mail.pop3.POP3SSLStore;
import com.sun.mail.pop3.POP3Store;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;  
import java.net.URL;  
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.DataHandler;
  
import org.apache.commons.mail.EmailAttachment;  
import org.apache.commons.mail.EmailException;  
import org.apache.commons.mail.HtmlEmail;  
import org.apache.commons.mail.MultiPartEmail;  
import org.apache.commons.mail.SimpleEmail;  
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
  
public class CommonsMail {  
    
    private Calendar now;
    
    public CommonsMail() throws EmailException, MalformedURLException {  
        System.out.println("Carregando email");
        //enviaEmailSimplesVirtual();  
        //enviaEmailComAnexo("teste");
        //testearquivo();
        System.out.println(""+checkHorario());
        /*try {
            abrirCaixaEmail();
            //enviaEmailComAnexo();
            //enviaEmailFormatoHtml();  
        } catch (MessagingException ex) {
            Logger.getLogger(CommonsMail.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(CommonsMail.class.getName()).log(Level.SEVERE, null, ex);
        }*/
    }  
    
    public void testearquivo(){
        File f = new File(System.getProperty("user.dir")+"/img/");
        System.out.println(""+f.list().length);
        String [] nome = f.list();
        for (int i = 0; i < nome.length; i++) {
            System.out.println(f.getAbsoluteFile()+"\\"+nome[i]);
        }
    }
    
    /** 
     * envia email simples(somente texto) 
     * @throws EmailException 
     */  
    public void enviaEmailSimples() throws EmailException {  
          
        SimpleEmail email = new SimpleEmail();  
        email.setHostName("smtp.gmail.com"); // o servidor SMTP para envio do e-mail  
        email.addTo("viniciusfragelli@hotmail.com", "Guilherme"); //destinatário  
        email.setFrom("viniciussenff@gmail.com", "Eu"); // remetente  
        email.setSubject("Teste -> Email simples"); // assunto do e-mail  
        email.setMsg("Teste de Email utilizando commons-email"); //conteudo do e-mail  
        email.setAuthentication("viniciussenff", "kld2h763");  
        //email.setSmtpPort(465);
        email.setSmtpPort(587);
        email.setSSL(false);  
        email.setTLS(true);  
        email.send();     
    }  
    
    public boolean getHorarioSabado(){
        if(now.get(Calendar.HOUR_OF_DAY) < 8 || now.get(Calendar.HOUR_OF_DAY) >= 19){
            return true;
        }else{
            return false;
        }
    }
    
    private boolean getHorarioSemana(){
        if(now.get(Calendar.HOUR_OF_DAY) < 7 || now.get(Calendar.HOUR_OF_DAY) >= 23){
            return true;
        }else{
            return false;
        }
    }
    
    public boolean checkHorario(){
        //sabado - 7
        //domingo - 1
        now  = Calendar.getInstance();
        if(now.get(Calendar.DAY_OF_WEEK) == 1){
            return true;
        }else{
            if(now.get(Calendar.DAY_OF_WEEK) == 7){
                return getHorarioSabado();
            }else{
                return getHorarioSemana();
            }
        } 
    }
    
    public void abrirCaixaEmail() throws MessagingException, IOException{
        String host = "mail.virtualtechnology.com.br";
        String username = "camera@virtualtechnology.com.br";
        String pass = "freepark"
                ;
        Properties properties = new Properties();
        properties.put("mail.pop3.host", host);
        Session emailSession = Session.getDefaultInstance(properties);

        POP3Store emailStore = (POP3Store) emailSession.getStore("pop3");
        emailStore.connect(username, pass);

        Folder emailFolder = emailStore.getFolder("INBOX");
        emailFolder.open(Folder.READ_WRITE);
        
        Message[] messages = emailFolder.getMessages();
        
        for (int i = 0; i < messages.length; i++){
            Message message = messages[i];
            if(message.getFrom()[0].toString().equalsIgnoreCase("camera@virtualtechnology.com.br") && checkHorario()){
                System.out.println("==============================");
                System.out.println("Email #" + (i + 1));
                System.out.println("Subject: " + message.getSubject());
                MimeBodyPart messageBodyPart = new MimeBodyPart();
                System.out.println("Text: " + message.getContent().toString());
                System.out.println("text: "+message.getFrom());
                Object objRef = message.getContent();   
                Multipart mp = (Multipart) objRef;
                int count = mp.getCount();
                String result= "";
                for (int j = 0; j < count; j++){
                    BodyPart bp = mp.getBodyPart( j );
                    System.out.println("count "+count+" "+bp.getContentType());
                    if (bp instanceof MimeBodyPart ){
                        MimeBodyPart mbp = (MimeBodyPart) bp;
                        if ( mbp.isMimeType( "text/plain" )) {
                            result = (String) mbp.getContent();
                        }else{
                            if(!mbp.isMimeType( "text/html" )){
                                byte[] buf = new byte[4096];  
                                String caminhoBase = System.getProperty("user.dir")+"/img/";
                                System.out.println(caminhoBase);
                                String nomeDoArquivo = bp.getFileName();  
                                if (nomeDoArquivo != null) {  
                                    InputStream is = bp.getInputStream();  
                                    FileOutputStream fos = new FileOutputStream(caminhoBase + nomeDoArquivo);  
                                    int bytesRead;  
                                    while ((bytesRead = is.read(buf)) != -1) {  
                                        fos.write(buf, 0, bytesRead);  
                                    }  
                                    fos.close();  
                                }  
                            }
                        } 
                    }
                }
                try {
                    enviaEmailComAnexo(result.replace("han", ""));
                    message.setFlag(Flags.Flag.DELETED, true);
                } catch (EmailException ex) {
                    Logger.getLogger(CommonsMail.class.getName()).log(Level.SEVERE, null, ex);
                }
            }else{
                message.setFlag(Flags.Flag.DELETED, true);
            }
        }
        
        emailFolder.close(true);
        emailStore.close();
    }
    
    public void enviaEmailSimplesVirtual() throws EmailException {  
          
        SimpleEmail email = new SimpleEmail();  
        email.setHostName("smtp.virtualtechnology.com.br"); // o servidor SMTP para envio do e-mail  
        email.addTo("viniciusfragelli@hotmail.com", "Guilherme"); //destinatário  
        email.setFrom("camera@virtualtechnology.com.br", "Eu"); // remetente  
        email.setSubject("Teste -> Email simples"); // assunto do e-mail  
        email.setMsg("Teste de Email utilizando commons-email"); //conteudo do e-mail  
        email.setAuthentication("camera@virtualtechnology.com.br", "freepark");  
        //email.setSmtpPort(465);
        email.setSmtpPort(587);
        email.setSSL(false);  
        email.setTLS(true);  
        email.send();     
    }
    
    public EmailAttachment[] getListaDeFotos(){
        File f = new File(System.getProperty("user.dir")+"/img/");
        String [] nome = f.list();
        if(nome.length == 0)return null;
        EmailAttachment [] lista = new EmailAttachment[nome.length];
        for (int i = 0; i < lista.length; i++) {
            lista[i] = new EmailAttachment();
            lista[i].setPath(f.getAbsoluteFile()+"\\"+nome[i]);
            lista[i].setDisposition(EmailAttachment.ATTACHMENT);
            lista[i].setDescription("foto"); 
            lista[i].setName(nome[i]);
        }
        return lista;
    }
    
    /** 
     * envia email com arquivo anexo 
     * @throws EmailException 
     */  
    public void enviaEmailComAnexo(String msg) throws EmailException{  
        
        
        
        // configura o email  
        MultiPartEmail email = new MultiPartEmail();  
        email.setHostName("smtp.virtualtechnology.com.br"); // o servidor SMTP para envio do e-mail  
        email.addTo("viniciusfragelli@hotmail.com", "Vinicius"); //destinatário  
        email.setFrom("camera@virtualtechnology.com.br", "Cameras"); // remetente  
        email.setSubject("Alerta cameras!"); // assunto do e-mail  
        email.setMsg(msg); //conteudo do e-mail  
        email.setAuthentication("camera@virtualtechnology.com.br", "freepark");  
        email.setSmtpPort(587);  
        email.setSSL(false);  
        email.setTLS(true);  
          
        // adiciona arquivo(s) anexo(s)  
        EmailAttachment [] lista = getListaDeFotos();
        if(lista != null){
            for (int i = 0; i < lista.length; i++) {
                email.attach(lista[i]);
            }
        }
        // envia o email  
        email.send();
        if(lista != null){
            apagarFotos();
        }
    }  
      
    private void apagarFotos(){
        File f = new File(System.getProperty("user.dir")+"/img/");
        File [] nome = f.listFiles();
        for (int i = 0; i < nome.length; i++) {
            nome[i].delete();
        }
    }
      
    /** 
     * Envia email no formato HTML 
     * @throws EmailException  
     * @throws MalformedURLException  
     */  
    public void enviaEmailFormatoHtml() throws EmailException, MalformedURLException {  
          
        HtmlEmail email = new HtmlEmail();  
          
        // adiciona uma imagem ao corpo da mensagem e retorna seu id  
        URL url = new URL("http://www.apache.org/images/asf_logo_wide.gif");  
        String cid = email.embed(url, "Apache logo");     
          
        // configura a mensagem para o formato HTML  
        email.setHtmlMsg("<html>Logo do Apache - <img ></html>");  
  
        // configure uma mensagem alternativa caso o servidor não suporte HTML  
        email.setTextMsg("Seu servidor de e-mail não suporta mensagem HTML");  
          
        email.setHostName("smtp.gmail.com"); // o servidor SMTP para envio do e-mail  
        email.addTo("teste@gmail.com", "Guilherme"); //destinatário  
        email.setFrom("teste@gmail.com", "Eu"); // remetente  
        email.setSubject("Teste -> Html Email"); // assunto do e-mail  
        email.setMsg("Teste de Email HTML utilizando commons-email"); //conteudo do e-mail  
        email.setAuthentication("teste", "xxxxx");  
        email.setSmtpPort(465);  
        email.setSSL(true);  
        email.setTLS(true);  
        // envia email  
        email.send();  
    } 
}