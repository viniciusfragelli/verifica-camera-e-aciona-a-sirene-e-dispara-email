/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Rotinas;

import com.sun.mail.pop3.POP3Store;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.BodyPart;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.internet.MimeBodyPart;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.MultiPartEmail;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 *
 * @author vinicius
 */
public class VerificaEmailCameras implements Job{
    
    private Calendar now;
    
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        try {
            abrirCaixaEmail();
        } catch (MessagingException ex) {
            Logger.getLogger(VerificaEmailCameras.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(VerificaEmailCameras.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private boolean getHorarioSabado(){
        if(now.get(Calendar.HOUR_OF_DAY) < 8 || now.get(Calendar.HOUR_OF_DAY) >= 19){
            return true;
        }else{
            return false;
        }
    }
    
    private boolean getHorarioSemana(){
        if(now.get(Calendar.HOUR_OF_DAY) < 7 || now.get(Calendar.HOUR_OF_DAY) >= 23){
            return true;
        }else{
            return false;
        }
    }
    
    private boolean checkHorario(){
        //sabado - 7
        //domingo - 1
        now  = Calendar.getInstance();
        if(now.get(Calendar.DAY_OF_WEEK) == 1){
            return true;
        }else{
            if(now.get(Calendar.DAY_OF_WEEK) == 7){
                return getHorarioSabado();
            }else{
                return getHorarioSemana();
            }
        } 
    }
    
    private void abrirCaixaEmail() throws MessagingException, IOException{
        String host = "mail.virtualtechnology.com.br";
        String username = "camera@virtualtechnology.com.br";
        String pass = "freepark";
        Properties properties = new Properties();
        properties.put("mail.pop3.host", host);
        Session emailSession = Session.getDefaultInstance(properties);
        POP3Store emailStore = (POP3Store) emailSession.getStore("pop3");
        emailStore.connect(username, pass);
        Folder emailFolder = emailStore.getFolder("INBOX");
        emailFolder.open(Folder.READ_WRITE);
        Message[] messages = emailFolder.getMessages();
        boolean teste = false;
        for (int i = 0; i < messages.length; i++){
            Message message = messages[i];
            if(message.getFrom()[0].toString().equalsIgnoreCase("camera@virtualtechnology.com.br") && checkHorario()){
                teste = true;
                Multipart mp = (Multipart) message.getContent();
                String result = capturaMsgESalvaFotos(mp);
                try {
                    enviaEmailComAnexo(result.replace("han", ""));
                    message.setFlag(Flags.Flag.DELETED, true);
                } catch (EmailException ex) {
                    Logger.getLogger(VerificaEmailCameras.class.getName()).log(Level.SEVERE, null, ex);
                }
            }else{
                message.setFlag(Flags.Flag.DELETED, true);
            }
        }
        if(teste){
            
        }
        emailFolder.close(true);
        emailStore.close();
    }
    
    private String capturaMsgESalvaFotos(Multipart mp) throws MessagingException, IOException{
        String result = "";
        int count = mp.getCount();
        for (int j = 0; j < count; j++){
            BodyPart bp = mp.getBodyPart(j);
            if (bp instanceof MimeBodyPart ){
                MimeBodyPart mbp = (MimeBodyPart) bp;
                if ( mbp.isMimeType( "text/plain" )) {
                    result = (String) mbp.getContent();
                }else{
                    if(!mbp.isMimeType( "text/html" )){
                        byte[] buf = new byte[4096];  
                        String caminhoBase = System.getProperty("user.dir")+"/img/";
                        System.out.println(caminhoBase);
                        String nomeDoArquivo = bp.getFileName();  
                        if (nomeDoArquivo != null) {  
                            InputStream is = bp.getInputStream();  
                            FileOutputStream fos = new FileOutputStream(caminhoBase + nomeDoArquivo);  
                            int bytesRead;  
                            while ((bytesRead = is.read(buf)) != -1) {  
                                fos.write(buf, 0, bytesRead);  
                            }  
                            fos.close();  
                        }  
                    }
                } 
            }
        }
        return result;
    }
    
    private void enviaEmailComAnexo(String msg) throws EmailException{  
        
        
        
        // configura o email  
        MultiPartEmail email = new MultiPartEmail();  
        email.setHostName("smtp.virtualtechnology.com.br"); // o servidor SMTP para envio do e-mail  
        email.addTo("viniciusfragelli@hotmail.com", "Vinicius"); //destinatário  
        email.setFrom("camera@virtualtechnology.com.br", "Cameras"); // remetente  
        email.setSubject("Alerta cameras!"); // assunto do e-mail  
        email.setMsg(msg); //conteudo do e-mail  
        email.setAuthentication("camera@virtualtechnology.com.br", "freepark");  
        email.setSmtpPort(587);  
        email.setSSL(false);  
        email.setTLS(true);  
          
        // adiciona arquivo(s) anexo(s)  
        EmailAttachment [] lista = getListaDeFotos();
        if(lista != null){
            for (int i = 0; i < lista.length; i++) {
                email.attach(lista[i]);
            }
        }
        // envia o email  
        email.send();
        if(lista != null){
            apagarFotos();
        }
    }  
      
    private void apagarFotos(){
        File f = new File(System.getProperty("user.dir")+"/img/");
        File [] nome = f.listFiles();
        for (int i = 0; i < nome.length; i++) {
            nome[i].delete();
        }
    }
   
    private EmailAttachment[] getListaDeFotos(){
        File f = new File(System.getProperty("user.dir")+"/img/");
        String [] nome = f.list();
        if(nome.length == 0)return null;
        EmailAttachment [] lista = new EmailAttachment[nome.length];
        for (int i = 0; i < lista.length; i++) {
            lista[i] = new EmailAttachment();
            lista[i].setPath(f.getAbsoluteFile()+"\\"+nome[i]);
            lista[i].setDisposition(EmailAttachment.ATTACHMENT);
            lista[i].setDescription("foto"); 
            lista[i].setName(nome[i]);
        }
        return lista;
    }
    
}
